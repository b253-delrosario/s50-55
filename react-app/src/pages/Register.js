import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register(){

	const { user } = useContext(UserContext);

	// State hooks to store the values of the input fields
	// getters are variables that store data (from setters)
	// setters are function that sets the data (for the getters)
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);

	// Function to simulate user registration
	function registerUser(e) {
		// Prevents page redirection via form submission
		e.preventDefault();

		const checkEmail = () => {
			fetch('http://localhost:4000/users/checkEmail', {
				method: 'POST',
				header: {
					"content-type": "application/json"
				},
				body: JSON.stringify({
					email: email
				})
			})
			.then(res => res.json())
			.then(data => {
				if(!data) {
					fetch('http://localhost:4000/users/register',
					{
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						},
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							email: email,
							mobileNumber: mobileNumber,
							password1: password1,
							password2: password2,
						})
					})
					.then(res => res.json())
					.then(data => {
						if(data) {
							setEmail('');
							setPassword1('');
							setPassword2('');
							setMobileNumber('');
							setFirstName('');
							setLastName('');

							Swal.fire({
								title: "Registration Successful",
								icon: "success",
								text: "Welcome to Zuitt!"
							});
						}
					}
		
				} else {

					Swal.fire({
						title: "Duplicate Email Found",
						icon: "error",
						text: "Please provide a different email."
					});
			}
		});
		// Clear input fields


		// alert('Thank you for registering!');
	}

	useEffect(() => {
		// Validation to enable submit button when all fields are populated and both passwords match
		if((email !== '' && password1 !== '' && password2 !== '' && firstName !== '' && lastName !== '' && mobileNumber !== '' ) && (password1 === password2)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password1, password2, lastName, firstName, mobileNumber]);

	return(

		(user.id !== null) ?
		    <Navigate to="/courses" />
		:
		// 2-way Binding (Bnding the user input states into their corresponding input fields via the onChange JSX Event Handler)
		<Form onSubmit={(e) => registerUser(e)} className="my-3">

			<Form.Group className="mb-3" controlId="firstName">
			  <Form.Label>First Name</Form.Label>
			  <Form.Control 
			  	type="text" 
			  	placeholder="Enter First Name"
			  	value={ firstName }
			  	onChange={e => setFirstName(e.target.value)}
			  	required/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="lastName">
			  <Form.Label>Last Name</Form.Label>
			  <Form.Control 
			  	type="text" 
			  	placeholder="Enter Last Name"
			  	value={ lastName }
			  	onChange={e => setLastName(e.target.value)}
			  	required/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="userEmail">
			<Form.Label>Email Address</Form.Label>
			<Form.Control 
				type="email" 
				placeholder="Enter Email"
				value={ email }
				onChange={e => setEmail(e.target.value)}
				required/>
			<Form.Text className="text-muted">
			  We'll never share your email with anyone else.
			</Form.Text>
			</Form.Group>

			<Form.Group className="mb-3" controlId="mobileNumber">
			<Form.Label>Mobile Number</Form.Label>
			<Form.Control 
				type="text" 
				placeholder="Enter Mobile Number"
				value={ mobileNumber }
				onChange={e => setMobileNumber(e.target.value)}
				required/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="password1">
			<Form.Label>Password</Form.Label>
			<Form.Control 
				type="password" 
				placeholder="Password"
				value={ password1 }
				onChange={e => setPassword1(e.target.value)} 
				required/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="password2">
			<Form.Label>Verify Password</Form.Label>
			<Form.Control 
				type="password" 
				placeholder="Verify Password"
				value={ password2 }
				onChange={e => setPassword2(e.target.value)}  
				required/>
			</Form.Group>

			{ isActive ?
					<Button variant="primary" type="submit" id="submitBtn">
					  Submit
					</Button>
					:
					<Button variant="danger" type="submit" id="submitBtn" disabled>
					  Submit
					</Button>
				}
			</Form>

	)
}